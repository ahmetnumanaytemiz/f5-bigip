## F5-BIGIP-UPGRADE-NOTES

> Ahmet Numan Aytemiz , 19 March 2021

---

I simply demontrate, how can we upgrades our Big IP System (Standalone) from 14.1.2.6 to 14.1.4

- Step 1 : Step 1: Backing Up Your Current Configuration and Download ucs
- Step 2:  Dowloand and install new big ip iso from download.f5.com
- Step 3: Install Available Image to Installed Image with new Boot Location
- Step 4:  Re-activate F5 License
- Step 5: Copy Running Configuration to New Boot Location
- Step 6 : Reboot F5 with new boot location
