## F5 ASM (WAF) Learning Plan 

>> Ahmet Numan Aytemiz , 25 Ocak 2020

---

Bu çalışmamım amacı kendi F5 ASM (WAF) bilgilerimi, hands on lab yaparak arttırmaktır. 

F5 ASM (WAF) üzerinde sıra ile aşağıdaki lab'lar yapılacaktır.

**Lab 1: Simple Security Policy Oluşturma ve L7 DOS Koruması** 

Bu lab'da 10.1.10.35 ip adresi üzerinde çalışan dvwa uygulamasını, sqi injection - Cross Site Scripting web ataklarından korumak için , ve layer 7 bot ataklarından korumak için
 - Rapid Reployment Application Security Policy'si oluşturulacak
 - Bot Defense Profili oluşturulacak 
 - Atakların, oluşturulan profiller virtual server'a assign edilmeden önce ve assign edildikten sonra uygulama üzerindeki etkileri incelenecek.

 **Lab 2 : Block Common Web Vulnerabilites with Big IP ASM WAF**
  - Command Execution
  - SQL injection
  - XSS
  - Forcefull Browsing
  - Create Comprehensive Security Policy
  - Create Learning Suggestions with IMacro
  - Block and log all these web attacks...










